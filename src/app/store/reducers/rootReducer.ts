import {
    ActionReducer,
    ActionReducerMap,
    createFeatureSelector,
    createSelector,
    MetaReducer
  } from '@ngrx/store';

  
  import * as userReducer from '../reducers/userreducer';
  
  export interface AppState {
    
   Users : userReducer.UserState
  }
  
  export const reducers: ActionReducerMap<AppState> = {
    Users : userReducer.Userreducer
  };