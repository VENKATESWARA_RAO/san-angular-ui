import * as userAction from '../actions/useraction';
import { on, createReducer } from '@ngrx/store';

export const counterFeatureKey = 'counter';

export interface UserState {
    Users: any[];
}

export const initialState: UserState = {
    Users: ['King', 'Queen','Solider', 'war','Castle']
};

export const Userreducer = createReducer(
  initialState,
  on(userAction.getUsers, (state) => ({ ...state})),
  on(userAction.updateorAddUsers, (state,{Users}) => ({ ...state, Users}))

);