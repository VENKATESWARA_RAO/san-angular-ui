import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';

import { DashbaordComponent } from './Pages/dashbaord/dashbaord.component';
import { NavBarComponentComponent } from './Components/nav-bar-component/nav-bar-component.component';
import { NavFooterComponent } from './Components/nav-footer/nav-footer.component';
import { AddUserComponent } from './Components/add-user/add-user.component';
import { UserListComponent } from './Components/user-list/user-list.component';
import { SeparatorComponent } from './Components/separator/separator.component';
import { FormsModule } from '@angular/forms';
import { EditableComponentComponent } from './Components/editable-component/editable-component.component';
import { MetaReducer, StoreModule } from '@ngrx/store';
import { environment } from 'src/environments/environment';
import { reducers } from './store/reducers/rootReducer';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

export const metaReducers: MetaReducer<any>[] = !environment.production
  ? []
  : [];
  
@NgModule({
  declarations: [
    AppComponent,

    DashbaordComponent,

    NavBarComponentComponent,

    NavFooterComponent,

    AddUserComponent,

    UserListComponent,

    SeparatorComponent,

    EditableComponentComponent,
  ],
  imports: [BrowserModule, FormsModule,
    StoreModule.forRoot(reducers, { metaReducers }),
    StoreDevtoolsModule.instrument({
      maxAge: 25, // Retains last 25 states
      logOnly: environment.production, // Restrict extension to log-only mode
    }),
  ],
  providers: [],
  
  bootstrap: [AppComponent],
})
export class AppModule {}
