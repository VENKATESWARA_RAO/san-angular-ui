import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-editable-component',
  templateUrl: './editable-component.component.html',
  styleUrls: ['./editable-component.component.scss'],
})
export class EditableComponentComponent implements OnInit {
  public isEditing: boolean;
  public pendingValue: string;
  @Input() User!: string;
  @Output() EditUser = new EventEmitter<string>();
  @Output() deleteUser = new EventEmitter<any>();

  constructor() {
    this.isEditing = false;
    this.pendingValue = undefined;
  }
  ngOnInit() {}

  edit() {
    this.pendingValue = this.User;
    this.isEditing = true;
  }

  editUser() {
    if (this.pendingValue !== this.User) {
      this.EditUser.emit(this.pendingValue);
    }

    this.isEditing = false;
  }

  removeUser(User) {
    this.deleteUser.emit(User);
  }
}
