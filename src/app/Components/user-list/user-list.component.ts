import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {
 @Input() Users?: any; 
 @Output() RemoveUser = new EventEmitter<any>();
 @Output() UpdateUser = new EventEmitter<any>();
  constructor() { }
  storeEditUser = undefined;
  showEdit = false;
  model = {User: undefined}
  ngOnInit(): void {
  }

  deleteUser(User) {
    this.RemoveUser.emit(User);
  }



  EditUser( OldUser, newUser: string )  {
   const data = {OldUser: OldUser,newUser:newUser}
   this.UpdateUser.emit(data);
		

	}


}
