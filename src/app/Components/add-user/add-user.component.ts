import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss'],
})
export class AddUserComponent implements OnInit {
  // @Output() AddUser?: new EventEmitter<any>();
  @Output() AddUser = new EventEmitter<any>();
  
  model = {User: undefined}

  constructor() {}
  
  ngOnInit(): void {}

  Add() {

    
    this.AddUser.emit(this.model);
   
  }
}
