import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { getUsers, updateorAddUsers } from 'src/app/store/actions/useraction';
import { AppState } from 'src/app/store/reducers/rootReducer';

@Component({
  selector: 'app-dashbaord',
  templateUrl: './dashbaord.component.html',
  styleUrls: ['./dashbaord.component.scss']
})

export class DashbaordComponent implements OnInit {
 
  constructor( private store: Store<AppState>) {
  
   }
 Users = ['King', 'Queen','Solider', 'war','Castle'];
  
  ngOnInit(): void {
    
  }

  AddUser(newUsers) {
    
    this.Users = [...this.Users, newUsers.User]; 
    this.store.dispatch(updateorAddUsers({ Users: this.Users }));
 
  }

  RemoveUser(User) {
    this.Users  = this.Users.filter(user => user !==User);
    this.store.dispatch(updateorAddUsers({ Users: this.Users }));

  }

  EditUser(User) {
    const removedUsers  = this.Users.filter(user => user !==User.OldUser);
    this.Users = [...removedUsers, User.newUser]; 
    this.store.dispatch(updateorAddUsers({ Users: this.Users }));
  }

}
